//
//  main.c
//  file_handling
//
//  Created by Sanket on 11/16/19.
//  Copyright © 2019 Sanket. All rights reserved.
//

#include <stdio.h>
#include <string.h>


int main()
{
    FILE *fptr;
    char a[10],b;
    printf("Enter the file name\n");
    gets(a);
    strcat(a,".txt");
    puts(a);
    fptr=fopen(a,"w");
    printf("Enter the content to file\n");
    while((b=getchar())!=EOF)
    {
        putc(b,fptr);
    }
    fclose(fptr);
    fptr=fopen(a,"r");
    while((b=getc(fptr))!=EOF)
    {
        printf("%c",b);
    }
    return 0;
}
