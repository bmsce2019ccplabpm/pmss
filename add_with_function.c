//
//  main.c
//  vowel
//
//  Created by Sanket on 9/22/19.
//  Copyright © 2019 Sanket. All rights reserved.
//

#include <stdio.h>
int input()
{
    int x;
    printf("Enter a number\n");
    scanf("%d",&x);
    return x;
}
int add(int a,int b)
{
    return a+b;
}
void output(int a,int b,int c)
{
    printf("%d + %d=%d\n",a,b,c);
}
int main()
{
    int a,b,c;
    a=input();
    b=input();
    c=add(a,b);
    output(a,b,c);
    return 0;
}
