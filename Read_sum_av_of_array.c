#include <stdio.h>
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}
int add(int a,int arr[a])
{
    int sum,i;
    for(i=0;i<a;i++)
        sum=sum+arr[i];
    return sum;
}
void output(int sum,int av)
{
    printf("The sum of the ellements of array is %d\n",sum);
    printf("Average of the elements is %d\n",av);
}
int main()
{
	int a=input();
    int arr[a],i,sum,av;
    printf("enter values in array\n");
    for(i=0;i<a;i++)
        scanf("%d",&arr[i]);
    sum=add(a,arr);
    av=sum/a;
    output(sum,av);
	return 0;
}
