//
//  main.c
//  vowel
//
//  Created by Sanket on 9/22/19.
//  Copyright © 2019 Sanket. All rights reserved.
//

#include <stdio.h>
int input()
{
    int x;
    printf("Enter a year\n");
    scanf("%d",&x);
    return x;
}
void check_leap(int a)
{
    if((a%4==0)&&(a%100!=0))
        printf("%d is leap year\n",a);
    else if(a%400==0)
        printf("%d is a leap year\n",a);
    else
        printf("%d is not a leap year\n",a);}
int main()
{
    int a;
    a=input();
    check_leap(a);
    return 0;
}
