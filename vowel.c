//
//  main.c
//  vowel
//
//  Created by Sanket on 9/22/19.
//  Copyright © 2019 Sanket. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    char c;
    printf("Enter a leter\n");
    scanf(" %c",&c);
    switch(c)
    {
        case 'a':
        case 'A':
            printf("%c is a vowel\n",c);
            break;
        case 'e':
        case'E':
            printf("%c is a vowel\n",c);
            break;
        case 'i':
        case 'I':
            printf("%c is a vowel\n",c);
            break;
        case 'o':
        case 'O':
            printf("%c is a consonant\n",c);
            break;
        case 'u':
        case 'U':
            printf("%c is a vowel\n",c);
            break;
        default:
            printf("%c is not a vowel\n",c);
    }
    return 0;
}
