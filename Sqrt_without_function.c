//
//  main.c
//  20_11_2019
//
//  Created by Sanket on 11/20/19.
//  Copyright © 2019 Sanket. All rights reserved.
//

#include <stdio.h>

int main()
{
    int n;
    float i,s;
    printf("Enter a number\n");
    scanf("%d",&n);
    s=n/2;
    i=0;
    while(s!=i)
    {
        i=s;
        s=((n/i)+i)/2;
    }
    printf("The squareroot of %d = %f\n",n,s);
    return 0;
}
