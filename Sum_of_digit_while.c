#include<stdio.h>
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}
int sum1(int a)
{
    int b,s=0;
    while(a>0)
    {
        b=a%10;
        s=s+b;
        a=a/10;
    }
    return s;
}
void output(int a,int b)
{
    printf("The sum of digit of %d =%d\n",a,b);
}
int main()
{
    int a,sum;
    a=input();
    sum=sum1(a);
    output(a,sum);
    return 0;
}