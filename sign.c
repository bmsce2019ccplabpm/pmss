#include <stdio.h>
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}
char check(int a)
{
    if(a>0)
        return '+';
    else if(a<0)
        return '-';
    else
        return '0';
}
void output(int a,char b)
{
    printf("%d is %c\n",a,b);
}
int main()
{
    int a=input();
    char b=check(a);
    output(a,b);
    return 0;
}
