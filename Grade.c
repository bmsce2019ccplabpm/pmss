//
//  main.c
//  vowel
//
//  Created by Sanket on 9/22/19.
//  Copyright © 2019 Sanket. All rights reserved.
//

#include <stdio.h>
int main()
{
    int a,val = 10;
    printf("Enter marks\n");
    scanf("%d",&a);
    if(a>=90)
        val=0;
    if((a>70)&&(a<90))
        val=1;
    if((a>60)&&(a<70))
        val=2;
    if((a>40)&&(a<60))
        val=3;
    
    switch(val)
    {
        case 0:
            printf("Grade A\n");
            break;
        case 1:
            printf("Grade B\n");
            break;
        case 2:
            printf("Grade C\n");
            break;
        case 3:
            printf("Grade D\n");
            break;
        default:
            printf("Fail\n");
            break;
        
    }
    return 0;
}
