#include <stdio.h>
#include <math.h>
float root1(int a,int b,int c)
{
float d=(-b+sqrt(b*b-4*a*c))/(2*a);
return d;
}
float root2(int a,int b,int c)
{
float d=(-b-sqrt(b*b-4*a*c))/(2*a);
return d;
}
int main()
{
int a,b,c;
float r1,r2;
printf("Enter the cofficient of quadratic eq.\n");
scanf("%d %d %d",&a,&b,&c);
if(b*b-4*a*c<0)
{
	printf("No real root\n");
}
else
{
r1=root1(a,b,c);
r2=root2(a,b,c);
printf("Root of the quadratic eq. are %f &%f",r1,r2);
}
return 0;
}