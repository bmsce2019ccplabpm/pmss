//
//  main.c
//  Swaping_with_pointer
//
//  Created by Sanket on 11/13/19.
//  Copyright © 2019 Sanket. All rights reserved.
//

#include <stdio.h>
void input(int *a)
{
    printf("Enter a number\n");
    scanf("%d",a);
}
void swap(int *p,int *q)
{
    int i;
    i=*p;
    *p=*q;
    *q=i;
}

int main(int argc, const char * argv[]) {
    int a,b;
    input(&a);
    input(&b);
    printf("before swap a=%d b=%d\n",a,b);
    swap(&a,&b);
    
    printf("After swap a=%d b=%d\n",a,b);
    return 0;
}
