//
//  minimum.c
//  Test
//
//  Created by Sanket on 9/18/19.
//  Copyright © 2019 Sanket. All rights reserved.
//

#include <stdio.h>
int input(void);
void output(int a,int b,int c,int x);
int min(int x,int y,int z);
int input()
{
    int a;
    printf("Entera number\n");
    scanf("%d",&a);
    return a;
}
void output(int a,int b,int c,int x)
{
    printf("Amoung %d, %d & %d %d is minimum\n",a,b,c,x);
}
int min(int x,int y,int z)
{   
    return ((x<y)&&(x<z))?x:(y<z)?y:z;
}
int main(void)
{
    int a,b,c,x;
    a=input();
    b=input();
    c=input();
    x=min(a,b,c);
    output(a,b,c,x);
    return 0;
}